package testmaven;
%%
%class AnalizadorJava
%public
%standalone
%unicode
RESERVADA_JAVA = "abstract"|"assert"|"boolean"|"byte"|"const"|"continue"|"default"|"enum"|"extends"|"finally"|"goto"|"import"|"instanceof"|"interface"|"native"|"package"|"short"|"strictfp"|"synchronized"|"throw"|"transient"|"volatile"|"catch"|"case"|"break"|"char"|"class"|"double"|"do"|"else"|"final"|"float"|"for"|"if"|"implements"|"int"|"long"|"new"|"private"|"protected"|"public"|"return"|"static"|"super"|"switch"|"this"|"throws"|"try"|"void"|"while"|"true"|"false"|"null"
PUNTO   = \.
ENTERO  = [1-9][0-9]* | 0+
FLOAT = {ENTERO}{PUNTO}0*{ENTERO}
ID_JAVA = [:jletter:][:jletterdigit:]*
COMENTARIO = "//".*|"/*"([^"*/"]|\n)*"*/"|"/**"("\n*".*)*"\n**/"

%%
{ENTERO}            { System.out.print("ENTERO("+yytext() + ")"); }
{RESERVADA_JAVA}    { System.out.print("RESERVADA_JAVA("+yytext() + ")"); }
{ID_JAVA}           { System.out.print("ID_JAVA("+yytext() + ")"); }
{FLOAT}             { System.out.print("FLOAT("+yytext() + ")"); }
{COMENTARIO}        { System.out.print("COMENTARIO("+yytext() + ")"); }
.                   { }
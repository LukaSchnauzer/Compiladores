"""
Hermanos Mala Muerta:
	Juan Carlos Montero Santiago
	Brandon Padilla Ruiz
	Diana Valeria Gomez Lopez
"""

#Stack:
#---------------------------------
def push(s,x):
	s.append(x)

def pop(s):
	return s.pop(len(s)-1)

def peek(s):
	return s[len(s)-1]
#................................

#Transiciones del automata implementado como un diccionario
"""
trans = {0:{'a':1, 'd':7},
	  1:{'b':2},
       2:{'c':3},
       3:{'a':4,'d':7},
       4:{'b':5},
       5:{'c':6},
       6:{'d':7,'a':4}}
"""

trans = {0:{'a':1,'c':5},
		1:{'b':2},
		2:{'a':3, 'c':5},
		3:{'b':4},	
		4:{'a':3, 'c':5}}

automata = ({0,1,2,3,4,5,6,7},{'a','b','c','d'},trans,0,{2,5}) 

def acepta(trans,ini,fin,s):
    actual = ini
    for c in s:
        actual = (trans[actual])[c]
    return actual in fin

def Tokenizer(M,inp): #M es el automata
	failed_previously = {}

	for q in M[0]:
		for i in range(len(inp)+1):
			failed_previously[(q,i)]=False

	i=0
	j=0
	stack = []
	while True:
		q = M[3] #Estado inicial
		push(stack,("Bottom",i))

		while i<len(inp):
			try:
				if failed_previously[(q,i)]:break
				if q in M[4]:
					stack = []

				push(stack,(q,i))
				q = M[2][q][inp[i]]
				i = i+1
			except KeyError, e:break

		while not(q in M[4]):
			failed_previously[(q,i)] = True
			(q,i) = pop(stack)
			if q == "Bottom":
				print "Failure: Tokenization not possible "
				return

		print "Token: ",inp[j:i]
		j=i
		if i>=len(inp):
			print "Success"
			return

reader = open("entrada.txt",'r')
s = reader.read()
s = s[0:len(s)-1] #Quitando EOF
Tokenizer(automata,s)
reader.close()
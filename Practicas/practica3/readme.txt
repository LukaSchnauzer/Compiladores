Nosotros para correr la practica hacemos lo siguiente (en UBUNTU):

- Entramos desde la terminal al directorio de la practica i.e. .../Compiladores/Practicas/practica3

- Ejecutamos: byaccj -Jpackage=asintactico src/main/byaccj/ari.y
  Cambiar ari.y por ard.y si se quiere probar la gramatica derecha.

- Despues ejecutamos mvn initialize seguido de mvn package

- Ahora ejecutamos sudo cp target/Analisissintactico-1.0.jar asintactico.Parser

- Y por ultimo: sudo java -cp target/Analisissintactico-1.0.jar asintactico.Parser src/main/resources/test.txt

El resultado es la interpretacion de la expresion aritmetica y la pila de reconocimiento.

Con respecto a la prueba de 3-2+8:
Podemos ver que la precedencia con operadores del mismo nivel es a la izquierda en ambas gramaticas.

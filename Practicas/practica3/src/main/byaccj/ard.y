%{
  import java.lang.Math;
  import java.io.*;
%}

%token<ival> ENTERO
%token MENOS MAS POR DIV
%type<ival> E,T,F,input

%%
/* Gram�tica con recursi�n derecha */
input : E {dump_stacks(stateptr); $$ = $1; System.out.println("[OK] "+ $$  );} /*Izquierda*/
;

E : T MAS E   {dump_stacks(stateptr); $$ = $1 + $3;}
  | T MENOS E {dump_stacks(stateptr); $$ = $1 - $3;}
  | T

T : F POR T {dump_stacks(stateptr); $$ = $1 * $3;}
  | F DIV T {dump_stacks(stateptr); $$ = $1 / $3;}
  | F

F : ENTERO       {dump_stacks(stateptr); $$ = $1;}
  | MENOS ENTERO {dump_stacks(stateptr); $$ = -1*$2;}
%%
//mvn exec:java -Dexec.mainClass="asintactico.Parser" -Dexec.args="src/main/resources/test.txt"

/* Referencia a analizador l�xico */
private Nodos lexer;

private int yylex () {
    int yyl_return = -1;
    try {
      yyl_return = lexer.yylex();
    }
    catch (IOException e) {
      System.err.println("IO error :"+e);
    }
    return yyl_return;
}

/* Funci�n para reportar error */
public void yyerror (String error) {
    System.err.println ("[ERROR] La expresion aritmetica no esta bien formada");
    System.exit(1);
}

/* Constructor */
public Parser(Reader r) {
    lexer = new Nodos(r, this);
}

/* Creaci�n del parser e inicializaci�n del reconocimiento */
public static void main(String args[]) throws IOException {
    Parser parser = new Parser(new FileReader(args[0]));
    parser.yyparse();
}

package asintactico;
%%
%{
    private Parser parser;

    public Nodos (java.io.Reader r, Parser p){
    	   this(r);
    	   parser = p;
    }
%}
%class Nodos
%standalone
%public
%unicode
ENTERO  = [1-9][0-9]* | 0+
%%

"-"             { return parser.MENOS; }
"+"		{ return parser.MAS; }
"*"		{ return parser.POR; }
"/"		{ return parser.DIV; }
{ENTERO}        { parser.yylval = new ParserVal(Integer.parseInt(yytext())); return parser.ENTERO; }
.               { }

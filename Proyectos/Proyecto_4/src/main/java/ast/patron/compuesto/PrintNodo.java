package ast.patron.compuesto;
import ast.patron.visitante.*;

public class PrintNodo extends Compuesto
{

    public PrintNodo(Nodo l){
	super(l);
    }

    public int accept(Visitor v){
     	return v.visit(this);
    }
}

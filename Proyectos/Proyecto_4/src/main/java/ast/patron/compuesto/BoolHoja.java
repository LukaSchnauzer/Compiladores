package ast.patron.compuesto;
import ast.patron.visitante.*;

public class BoolHoja extends Hoja
{
    public BoolHoja(String i){
        if (i.equals("True"))
            valor = new Variable(true);
        else
            valor = new Variable(false);
	tipo = 1;
    }

    public int accept(Visitor v){
     	return v.visit(this);
    }
}

package ast.patron.compuesto;
import ast.patron.visitante.*;

public class LeqNodo extends NodoBinario
{

    public LeqNodo(Nodo l, Nodo r){
	super(l,r);
    }

    public int accept(Visitor v){
     	return v.visit(this);
    }
}

package ast.patron.compuesto;
import ast.patron.visitante.*;

public class WhileNodo extends NodoBinario
{

    public WhileNodo(Nodo l, Nodo r){
	super(l,r);
    }

    public int accept(Visitor v){
     	return v.visit(this);
    }
}

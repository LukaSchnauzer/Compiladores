package ast.patron.visitante;
import ast.patron.compuesto.*;
import java.util.LinkedList;
import java.util.Iterator;

public class VisitorPrint implements Visitor
{
    public int visit(ProdNodo n){
        System.out.println("[*]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        return 0;
    }
 
    public int visit(AddNodo n){
        System.out.println("[+]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        return 0;
    }
    public int visit(AsigNodo n){
        System.out.println("[=]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        return 0;
    }
    public int visit(Compuesto n){
        for (Iterator i = n.getHijos().iterator(); i.hasNext(); ) {
            Nodo hijo = (Nodo) i.next();
            System.out.print("[");
            hijo.accept(this);
            System.out.println("]");
        }
        return 0;
    }
    public int visit(DifNodo n){
        System.out.println("[-]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.print("]");
        return 0;
    }
   
    public int visit(DivENodo n){
        System.out.println("[//]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        return 0;
    }
    
    public int visit(ModNodo n){
        System.out.println("[%]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        return 0;
    }
    public int visit(DivNodo n){
        System.out.println("[/]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        return 0;
    }
    public int visit(PowNodo n){
        System.out.println("[**]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        return 0;
    }    
        
        public int visit(AndNodo n){
        System.out.println("[and]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        return 0;        
    }
    
    public int visit(DiffNodo n){
        System.out.println("[!=]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        return 0;
    }
    
    public int visit(EqNodo n){
        System.out.println("[==]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        return 0;
    }
    
    public int visit(GrNodo n){
        System.out.println("[>]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        return 0;
    }
    
    public int visit(GrqNodo n){
        System.out.println("[>=]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        return 0;
    }
    
    public int visit(LeqNodo n){
        System.out.println("[<=]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        return 0;
    }
    
    public int visit(LessNodo n){
        System.out.println("[<]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        return 0;
    }
    
    public int visit(NotNodo n){
        System.out.println("[not]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        return 0;
    }
    
    public int visit(PrintNodo n){
        System.out.println("[print]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        return 0;
    }
    
    public int visit(OrNodo n){
        System.out.println("[or]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        return 0;
    }
    
    public int visit(IfNodo n){
        System.out.println("[if]");
        if(n.getSizeHijos()==3){
            LinkedList<Nodo> hijos = n.getAll();
            System.out.print("[Cond: ");
            hijos.get(0).accept(this);
            System.out.print("]");
            System.out.print("[Then: ");
            hijos.get(1).accept(this);
            System.out.print("]");
            System.out.print("[Else: ");
            hijos.get(2).accept(this);
            System.out.print("]");
        }else{
            LinkedList<Nodo> hijos = n.getAll();
            System.out.print("[Cond: ");
            hijos.get(0).accept(this);
            System.out.print("]");
            System.out.print("[Then: ");
            hijos.get(1).accept(this);
            System.out.print("]");
        }
        return 0;
    }
    
    public int visit(WhileNodo n){
        System.out.println("[While]");
        System.out.print("[Cond: ");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[Do: ");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        return 0;
    }
    
    public int visit(Hoja n){
        return 0;
    }
    public int visit(IdentifierHoja n){
	System.out.print("[Hoja Identificador] id: "+ n.getNombre());
        return 0;
    }
    public int visit(IntHoja n){
	System.out.print("[Hoja Entera] valor: " + n.getValor().ival);
        return 0;
    }
    public int visit(BoolHoja n){
        System.out.print("[Hoja Booleana] valor: " + n.getValor().bval);
        return 0;
    }
    public int visit(RealHoja n){
        System.out.print("[Hoja Real] valor: " + n.getValor().dval);
        return 0;
    }
    public int visit(StringHoja n){
        System.out.print("[Hoja String] valor: \"" + n.getValor().sval+ "\"");
        return 0;
    }
    public int visit(Nodo n){
        return 0;
    }
    
    public int visit(NodoBinario n){
       return 0;
    }
    
    public int visit(NodoStmts n){
        return 0;
    }
}

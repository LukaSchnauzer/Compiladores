/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.patron.visitante;

import ast.patron.compuesto.*;
import java.util.Hashtable;

/**
 *
 * @author luka
 */
public class Tabla {
    Hashtable<String,Integer> tabla;
    
    public Tabla(){
        tabla = new Hashtable<String,Integer>();
    }
    
    public Integer lookup(String name){
        return tabla.get(name);
    }
    
    // name = nombre de la variable
    // value = tipo de la variable 
    // :: 0 = bool : 1 = int : 2= real : 3 = cadena ::
    public void insert(String name, int value){
        tabla.put(name,value);
    }
    
}

package ast.patron.visitante;
import ast.patron.compuesto.*;

public interface Visitor
{
    public int visit(AddNodo n);  //
    public int visit(AsigNodo n);
    public int visit(Compuesto n);
    public int visit(DifNodo n); 
    public int visit(Hoja n);
    public int visit(ProdNodo n);
    public int visit(DivENodo n);
    public int visit(DivNodo n);
    public int visit(ModNodo n);
    public int visit(PowNodo n); 
    public int visit(IdentifierHoja n);
    public int visit(IntHoja n);
    public int visit(BoolHoja n);
    public int visit(RealHoja n);
    public int visit(StringHoja n);
    public int visit(Nodo n);
    public int visit(NodoBinario n);
    public int visit(NodoStmts n);
    public int visit(AndNodo n);
    public int visit(DiffNodo n);
    public int visit(EqNodo n);
    public int visit(GrNodo n);
    public int visit(GrqNodo n);
    public int visit(LeqNodo n);
    public int visit(LessNodo n);
    public int visit(NotNodo n);
    public int visit(OrNodo n);
    public int visit(PrintNodo n);
    public int visit(IfNodo n);
    public int visit(WhileNodo n);    
}

//import Parser;
package ast;
import java.io.*;
import ast.patron.compuesto.*;
import ast.patron.visitante.*;


public class Compilador{

    Parser parser;
    Nodo raízAST;
    VisitorTipo v_tipo;
    VisitorPrint v_print;

    Compilador(Reader fuente){
        parser = new Parser(fuente);
        v_tipo = new VisitorTipo();
        v_print = new VisitorPrint();
    }

    public void ConstruyeAST(boolean debug){
        parser.yydebug = debug;
        parser.yyparse(); // análisis léxico, sintáctio y constucción del AST
        raízAST = parser.raíz;
    }

    public void revisaAST(){
        parser.raíz.accept(v_print);
        parser.raíz.accept(v_tipo);
    }

    public static void main(String[] args){
            String archivo = "src/main/resources/fizzbuzz.p";
        try{
            Reader a = new FileReader(archivo);
            Compilador c  = new Compilador(a);
            c.ConstruyeAST(true);
            c.revisaAST();
        }catch(FileNotFoundException e){
            System.err.println("El archivo " + archivo +" no fue encontrado. ");
        }catch(ArrayIndexOutOfBoundsException e){
            System.err.println("Uso: java Compilador [archivo.p]: ");
        }
    }
}

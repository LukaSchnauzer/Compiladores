package ast.patron.visitante;
import ast.patron.compuesto.*;
import java.util.LinkedList;
import java.util.Iterator;

public class VisitorPrint implements Visitor
{
    public void visit(ProdNodo n){
        System.out.println("[*]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
    }
 
    public void visit(AddNodo n){
        System.out.println("[+]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
    }
    public void visit(AsigNodo n){
        System.out.println("[=]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");

    }
    public void visit(Compuesto n){
        for (Iterator i = n.getHijos().iterator(); i.hasNext(); ) {
            Nodo hijo = (Nodo) i.next();
            System.out.print("[");
            hijo.accept(this);
            System.out.println("]");
        }

    }
    public void visit(DifNodo n){
        System.out.println("[-]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.print("]");
    }
   
    public void visit(DivENodo n){
        System.out.println("[//]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
    }
    public void visit(ModNodo n){
        System.out.println("[%]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
    }
    public void visit(DivNodo n){
        System.out.println("[/]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
    }
    public void visit(PowNodo n){
        System.out.println("[**]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
    }    
        
        public void visit(AndNodo n){
        System.out.println("[and]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        
    }
    
    public void visit(DiffNodo n){
        System.out.println("[!=]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        
    }
    
    public void visit(EqNodo n){
        System.out.println("[==]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        
    }
    
    public void visit(GrNodo n){
        System.out.println("[>]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        
    }
    
    public void visit(GrqNodo n){
        System.out.println("[>=]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        
    }
    
    public void visit(LeqNodo n){
        System.out.println("[<=]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        
    }
    
    public void visit(LessNodo n){
        System.out.println("[<]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        
    }
    
    public void visit(NotNodo n){
        System.out.println("[not]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        
    }
    
    public void visit(PrintNodo n){
        System.out.println("[print]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        
    }
    
    public void visit(OrNodo n){
        System.out.println("[or]");
        System.out.print("[");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
        
    }
    
    public void visit(IfNodo n){
        System.out.println("[if]");
        if(n.getSizeHijos()==3){
            LinkedList<Nodo> hijos = n.getAll();
            System.out.print("[Cond: ");
            hijos.get(0).accept(this);
            System.out.print("]");
            System.out.print("[Then: ");
            hijos.get(1).accept(this);
            System.out.print("]");
            System.out.print("[Else: ");
            hijos.get(2).accept(this);
            System.out.print("]");
        }else{
            LinkedList<Nodo> hijos = n.getAll();
            System.out.print("[Cond: ");
            hijos.get(0).accept(this);
            System.out.print("]");
            System.out.print("[Then: ");
            hijos.get(1).accept(this);
            System.out.print("]");
        }
    }
    
    public void visit(WhileNodo n){
        System.out.println("[While]");
        System.out.print("[Cond: ");
        n.getPrimerHijo().accept(this);
        System.out.print("]");
        System.out.print("[Do: ");
        n.getUltimoHijo().accept(this);
        System.out.println("]");
    }
    
    public void visit(Hoja n){

    }
    public void visit(IdentifierHoja n){
	System.out.print("[Hoja Identificador] id: "+ n.getNombre());
    }
    public void visit(IntHoja n){
	System.out.print("[Hoja Entera] valor: " + n.getValor().ival);
    }
    public void visit(BoolHoja n){
        System.out.print("[Hoja Booleana] valor: " + n.getValor().bval);
    }
    public void visit(RealHoja n){
        System.out.print("[Hoja Real] valor: " + n.getValor().dval);
    }
    public void visit(StringHoja n){
        System.out.print("[Hoja String] valor: \"" + n.getValor().sval+ "\"");
    }
    public void visit(Nodo n){

    }
    
    public void visit(NodoBinario n){
       

    }
    public void visit(NodoStmts n){
    }
}

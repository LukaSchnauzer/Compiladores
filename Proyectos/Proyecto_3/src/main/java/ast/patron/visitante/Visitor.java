package ast.patron.visitante;
import ast.patron.compuesto.*;

public interface Visitor
{
    public void visit(AddNodo n);  //
    public void visit(AsigNodo n);
    public void visit(Compuesto n);
    public void visit(DifNodo n); 
    public void visit(Hoja n);
    public void visit(ProdNodo n);
    public void visit(DivENodo n);
    public void visit(DivNodo n);
    public void visit(ModNodo n);
    public void visit(PowNodo n); 
    public void visit(IdentifierHoja n);
    public void visit(IntHoja n);
    public void visit(BoolHoja n);
    public void visit(RealHoja n);
    public void visit(StringHoja n);
    public void visit(Nodo n);
    public void visit(NodoBinario n);
    public void visit(NodoStmts n);
    public void visit(AndNodo n);
    public void visit(DiffNodo n);
    public void visit(EqNodo n);
    public void visit(GrNodo n);
    public void visit(GrqNodo n);
    public void visit(LeqNodo n);
    public void visit(LessNodo n);
    public void visit(NotNodo n);
    public void visit(OrNodo n);
    public void visit(PrintNodo n);
    public void visit(IfNodo n);
    public void visit(WhileNodo n);    
}

package ast.patron.compuesto;
import ast.patron.visitante.*;

public class PrintNodo extends Compuesto
{

    public PrintNodo(Nodo l){
	super(l);
    }

    public void accept(Visitor v){
     	v.visit(this);
    }
}

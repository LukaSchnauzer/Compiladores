package ast.patron.compuesto;
import ast.patron.visitante.*;
import java.util.LinkedList;

public class IfNodo extends Compuesto
{

    public IfNodo(){
	super();
    }
    
    public LinkedList<Nodo> getAll(){
        return hijos.getAll();
    }
    
    public int getSizeHijos(){
        return hijos.size();
    }

    public void accept(Visitor v){
     	v.visit(this);
    }
}

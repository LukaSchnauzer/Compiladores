package ast.patron.compuesto;
import ast.patron.visitante.*;

public class BoolHoja extends Hoja
{
    public BoolHoja(String i){
        if (i.equals("True"))
            valor = new Variable(true);
        else
            valor = new Variable(false);
	tipo = 1;
    }

    public void accept(Visitor v){
     	v.visit(this);
    }
}

package ast.patron.compuesto;
import ast.patron.visitante.*;

public class LeqNodo extends NodoBinario
{

    public LeqNodo(Nodo l, Nodo r){
	super(l,r);
    }

    public void accept(Visitor v){
     	v.visit(this);
    }
}

//import Parser;
package ast;
import java.io.*;
import ast.patron.compuesto.*;
import ast.patron.visitante.*;


public class Compilador{

    Parser parser;
    Nodo raízAST;
    VisitorTipo v_tipo;
    VisitorPrint v_print;
    VisitorGenerador v_gen;

    Compilador(Reader fuente){
        parser = new Parser(fuente);
        v_tipo = new VisitorTipo();
        v_print = new VisitorPrint();
        v_gen = new VisitorGenerador();
    }

    public void ConstruyeAST(boolean debug){
        parser.yydebug = debug;
        parser.yyparse(); // análisis léxico, sintáctio y constucción del AST
        raízAST = parser.raíz;
    }

    public String revisaAST(){
        //parser.raíz.accept(v_print);
        parser.raíz.accept(v_tipo);
        parser.raíz.accept(v_gen);
        String rv = v_gen.genera();
        System.out.println(rv);
        return rv;
    }

    public static void main(String[] args){
            String archivo = "src/main/resources/test.p";
            String output = "out.txt";
        try{
            PrintWriter writer = new PrintWriter(output);
            Reader a = new FileReader(archivo);
            Compilador c  = new Compilador(a);
            c.ConstruyeAST(true);
            writer.println(c.revisaAST());
            writer.close();
        }catch(FileNotFoundException e){
            System.err.println("El archivo " + archivo +" no fue encontrado. ");
        }catch(ArrayIndexOutOfBoundsException e){
            System.err.println("Uso: java Compilador [archivo.p]: ");
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.patron.visitante;

/**
 *
 * @author luka
 */
public class Tipos {
    
    //suma:
    /*
    * {0,0,0,0}
    * {0,1,2,3}
    * {0,2,2,3}
    * {0,3,3,3}
    */
    final int suma[][] = {{0,0,0,0},{0,1,2,3},{0,2,2,3},{0,3,3,3}}; // sirve para resta
    //prod:
    /*
    * {0,0,0,-1}
    * {0,1,2,-1}
    * {0,2,2,-1}
    * {-1,-1,-1,-1}
    */
    final int prod[][] = {{0, 0, 0, -1},{0, 1, 2, -1},{0, 2, 2, -1},{-1,-1,-1,-1}};; // sirve para div
    //divE:
    /*
    * {-1,-1,-1,-1}
    * {-1,1,1,-1}
    * {-1,1,1,-1}
    * {-1,-1,-1,-1}
    */
    final int divE[][] = {{-1, -1, -1, -1},{-1, 1, 1, -1},{-1, 1, 1, -1},{-1,-1,-1,-1}}; // para division entera
    //mod:
    /*
    * {-1,-1,-1,-1}
    * {-1,1,2,-1}
    * {-1,2,2,-1}
    * {-1,1,2,-1}
    */
    final int mod[][] = {{-1, -1, -1, -1},{-1, 1, 2, -1},{-1, 2, 2, -1},{-1,-1,-1,-1}};
    //pow:
    /*
     * {-1,-1,-1,-1}
    * {-1,1,2,-1}
    * {-1,2,2,-1}
    * {-1,1,2,-1}
    */
    final int pow[][] = {{-1, -1, -1, -1},{-1, 1, 2, -1},{-1, 2, 2, -1},{-1,-1,-1,-1}};
    //logOp:
    /*
    * {0,0,0,-1}
    * {0,1,2,-1}
    * {0,2,2,-1}
    * {-1,-1,-1,-1}
    */
    final int logOp[][] = {{0, 0, 0, -1},{0, 1, 2, -1},{0, 2, 2, -1},{-1,-1,-1,-1}}; //para operadores logicos or and, etc
    //cond:
    /*
    * {0,0,0,0}
    * {0,0,0,0}
    * {0,0,0,0}
    * {0,0,0,0}
    */
    final int cond[][] = {{0, 0, 0, 0},{0, 0, 0, 0},{0, 0, 0, 0},{0,0,0,0}}; //para "<" | ">" | "<=" | "+=" | "-=" | ">=" | "==" | "!=" | "<>" | "="
    
    // si una funcion regresa -1 es un error
    
    public int verificaSum(int i,int j){
        return suma[i][j];
    }
    
    public int verificaProd(int i,int j){
        return prod[i][j];
    }
    
    public int verificadivE(int i,int j){
        return divE[i][j];
    }
    
    public int verificaMod(int i,int j){
        return mod[i][j];
    }
    
    public int verificaPow(int i,int j){
        return pow[i][j];
    }
    
    public int verificaLogOp(int i,int j){
        return logOp[i][j];
    }
    
    public int verificaCond(int i,int j){
        return cond[i][j];
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.patron.visitante;

import ast.patron.compuesto.*;
import java.util.LinkedList;
import java.util.Iterator;
/**
 *
 * @author luka
 */
public class VisitorGenerador implements Visitor{
    String first = "main:\n.data\n";
    String second = ".text\n";
    
    Registros reg = new Registros();
    
    //Este solo se ejecuta despues de visitar todo el arbol
    public String genera(){
        return first + second + "end:\t jr $ra";
    }
    
    public int visit(AddNodo n){
        Nodo hi = n.getPrimerHijo();
      Nodo hd = n.getUltimoHijo();

      // Tipo de registro objetivo
      int tipo = n.getType();
      
      boolean entero;
      entero = tipo != 2;

      String objetivo = reg.getObjetivo(entero);
      String[] siguientes = reg.getNsiguientes(2,entero);

      // Genero el código del subárbol izquiero
      reg.setObjetivo(siguientes[0],entero);
      hi.accept(this);

      // Genero el código del subárbol derecho
      reg.setObjetivo(siguientes[1], entero);
      hd.accept(this);

      String opcode;
      if(tipo==2)
          opcode = "add.s";
      else
          opcode = "add";

      second += "\t" + opcode + " " + objetivo + ", " + siguientes[0] + ", " + siguientes[1]+"\n";
      
      return 0;
    }
    
    public int visit(DifNodo n){
      Nodo hi = n.getPrimerHijo();
      Nodo hd = n.getUltimoHijo();

      // Tipo de registro objetivo
      int tipo = n.getType();
      
      boolean entero;
      entero = tipo != 2;

      String objetivo = reg.getObjetivo(entero);
      String[] siguientes = reg.getNsiguientes(2,entero);

      // Genero el código del subárbol izquiero
      reg.setObjetivo(siguientes[0],entero);
      hi.accept(this);

      // Genero el código del subárbol derecho
      reg.setObjetivo(siguientes[1], entero);
      hd.accept(this);

      String opcode;
      if(tipo==2)
          opcode = "sub.s";
      else
          opcode = "sub";

      second += "\t" + opcode + " " + objetivo + ", " + siguientes[0] + ", " + siguientes[1]+"\n";
      
      return 0;
    } 
    
    public int visit(ProdNodo n){
      Nodo hi = n.getPrimerHijo();
      Nodo hd = n.getUltimoHijo();

      // Tipo de registro objetivo
      int tipo = n.getType();
      
      boolean entero;
      if (tipo==2)
          entero = false;
      else
          entero = true;

      String objetivo = reg.getObjetivo(entero);
      String[] siguientes = reg.getNsiguientes(2,entero);

      // Genero el código del subárbol izquiero
      reg.setObjetivo(siguientes[0],entero);
      hi.accept(this);

      // Genero el código del subárbol derecho
      reg.setObjetivo(siguientes[1], entero);
      hd.accept(this);

      String opcode;
      if(tipo==2)
          opcode = "mult.s";
      else
          opcode = "mult";

      second += "\t" + opcode + " " + siguientes[0] + ", " + siguientes[1]+"\n";
      
      return 0;
    }
    
    public int visit(DivNodo n){
      Nodo hi = n.getPrimerHijo();
      Nodo hd = n.getUltimoHijo();

      // Tipo de registro objetivo
      int tipo = n.getType();
      
      boolean entero;
      if (tipo==2)
          entero = false;
      else
          entero = true;

      String objetivo = reg.getObjetivo(entero);
      String[] siguientes = reg.getNsiguientes(2,entero);

      // Genero el código del subárbol izquiero
      reg.setObjetivo(siguientes[0],entero);
      hi.accept(this);

      // Genero el código del subárbol derecho
      reg.setObjetivo(siguientes[1], entero);
      hd.accept(this);

      String opcode;
      if(tipo==2)
          opcode = "div.s";
      else
          opcode = "div";

      second += "\t" + opcode + " " + siguientes[0] + ", " + siguientes[1]+"\n";
      
      return 0;
    }
    
    //Nodos como este que no tienen tipo tendran tipo 0 (bool) por simplicidad
    public int visit(AsigNodo n){
      String id = n.getPrimerHijo().getNombre();
      Nodo hd = n.getUltimoHijo();
      boolean mflo_needed = (hd.getClass().getSimpleName().equals("ProdNodo") || hd.getClass().getSimpleName().equals("DivNodo"));
      
      if(!first.contains("\t"+id+":")){
          first+="\t"+id+": .word 0\n";
      }
      // Tipo de registro objetivo
      int tipo = hd.getType();
      
      boolean entero;
      if (tipo==2)
          entero = false;
      else
          entero = true;

      String objetivo = reg.getObjetivo(entero);
      String[] siguientes = reg.getNsiguientes(1,entero);

      // Genero el código del subárbol derecho
      reg.setObjetivo(siguientes[0], entero);
      hd.accept(this);

      String opcode = "sw";
      if(mflo_needed)
          second += "\t" + "mflo "+objetivo+"\n";
      second += "\t" + opcode + " " + objetivo + ", " + id+"\n";
      
      return 0;
    }
    
    public int visit(PrintNodo n){
      Nodo hd = n.getPrimerHijo();
      
      // Tipo de registro objetivo
      int tipo = hd.getType();
      
      boolean entero;
      if (tipo==2)
          entero = false;
      else
          entero = true;
      
      String objetivo = reg.getObjetivo(entero);
      String[] siguientes = reg.getNsiguientes(1,entero);

      // Genero el código del subárbol derecho
      reg.setObjetivo(siguientes[0], entero);
      hd.accept(this);
      
      if (entero)
        second += "\t" + "move $a0, "+objetivo+"\n";
      else
        second += "\t" + "move $f12, "+objetivo+"\n";
      second += "\t" + "li $v0, 1"+"\n";
      second += "\t" + "syscall"+"\n";
      
      return 0;
    }
        
    public int visit(IntHoja n){
        second += "\t" + "li "+reg.getObjetivo(true)+","+n.getValor().ival+"\n";
        return 0;
    }
    
    public int visit(RealHoja n){
        second += "\t" + "li "+reg.getObjetivo(false)+","+n.getValor().dval+"\n";
        return 0;
    }
    
    public int visit(IdentifierHoja n){
        // Tipo de registro objetivo
      int tipo = n.getType();
      
      boolean entero;
      if (tipo==2)
          entero = false;
      else
          entero = true;
      
      second += "\t" + "lw "+reg.getObjetivo(entero)+","+n.getNombre()+"\n";
      
      return 0;
    }
    
    
    public int visit(Compuesto n){
        for (Iterator i = n.getHijos().iterator(); i.hasNext(); ) {
            Nodo hijo = (Nodo) i.next();
            hijo.accept(this);
            second += "\n"; 
        }
        return 0;
    }
    
    public int visit(BoolHoja n){return 0;}
    public int visit(StringHoja n){return 0;}
    public int visit(Hoja n){return 0;}
    public int visit(Nodo n){return 0;}
    public int visit(NodoBinario n){return 0;}
    public int visit(WhileNodo n){return 0;}
    public int visit(IfNodo n){return 0;}
    public int visit(NodoStmts n){return 0;}
    public int visit(AndNodo n){return 0;}
    public int visit(DiffNodo n){return 0;}
    public int visit(EqNodo n){return 0;}
    public int visit(GrNodo n){return 0;}
    public int visit(GrqNodo n){return 0;}
    public int visit(LeqNodo n){return 0;}
    public int visit(LessNodo n){return 0;}
    public int visit(NotNodo n){return 0;}
    public int visit(OrNodo n){return 0;}
    public int visit(ModNodo n){return 0;}
    public int visit(PowNodo n){return 0;}
    public int visit(DivENodo n){return 0;}
}

package ast.patron.visitante;
import ast.patron.compuesto.*;
import java.util.LinkedList;
import java.util.Iterator;

public class VisitorTipo implements Visitor{
    Tabla tabla;
    Tipos tipos;
    
    public VisitorTipo(){
        tabla = new Tabla();
        tipos = new Tipos();
    }
    
    public String toString(int i){
         String aux1;
         
         switch(i){
                case 0:
                    return "bool";
                case 1:
                    return "int";
                case 2:
                    return "real";
                case 3:
                    return "String";
            }
         return null;
    }
    
    public int visit(AddNodo n){
        int ti = n.getPrimerHijo().accept(this);
        int td = n.getUltimoHijo().accept(this);
        int this_tipo = this.tipos.verificaSum(ti, td);
        
        if (this_tipo == -1){
            //Error:
            System.out.println("Error de tipos:"+this.toString(ti)+" + "+this.toString(td));
            System.exit(0);
        }
        
        n.setTipo(this_tipo);
        return this_tipo;
    }
    
    //Nodos como este que no tienen tipo tendran tipo 0 (bool) por simplicidad
    public int visit(AsigNodo n){
        int t = n.getUltimoHijo().accept(this);
        String id = n.getPrimerHijo().getNombre();
        
        if ( this.tabla.lookup(id)==null ){ //el id no esta en la tabla (hay que meterlo)
            this.tabla.insert(id,t);
            return 0;
        }
        
        int tab = this.tabla.lookup(id);
        //si llega aqui el id ya está en la tabla
        //revisar si el tipo no ha cambiado
        if (tab != t){ //estan intentando cambiar el tipo de la variable
            System.out.println("Error de tipos:"+this.toString(tab)+" --> "+this.toString(t));
            System.exit(0);
        }
        //si llega aqui el tipo no cambio, no hay que hacer mas
        return 0;
    }
    
    public int visit(DifNodo n){
        int ti = n.getPrimerHijo().accept(this);
        int td = n.getUltimoHijo().accept(this);
        int this_tipo = this.tipos.verificaSum(ti, td);
        
        if (this_tipo == -1){
            //Error:
            System.out.println("Error de tipos:"+this.toString(ti)+" - "+this.toString(td));
            System.exit(0);
        }
        
        n.setTipo(this_tipo);
        return this_tipo;
    } 
    
    public int visit(ProdNodo n){
        int ti = n.getPrimerHijo().accept(this);
        int td = n.getUltimoHijo().accept(this);
        int this_tipo = this.tipos.verificaProd(ti, td);
        
        if (this_tipo == -1){
            //Error:
            System.out.println("Error de tipos:"+this.toString(ti)+" * "+this.toString(td));
            System.exit(0);
        }
        
        n.setTipo(this_tipo);
        return this_tipo;
    }
    
    public int visit(DivENodo n){
        int ti = n.getPrimerHijo().accept(this);
        int td = n.getUltimoHijo().accept(this);
        int this_tipo = this.tipos.verificadivE(ti, td);
        
        if (this_tipo == -1){
            //Error:
            System.out.println("Error de tipos:"+this.toString(ti)+" // "+this.toString(td));
            System.exit(0);
        }
        
        n.setTipo(this_tipo);
        return this_tipo;
    }
    
    public int visit(DivNodo n){
        int ti = n.getPrimerHijo().accept(this);
        int td = n.getUltimoHijo().accept(this);
        int this_tipo = this.tipos.verificaProd(ti, td);
        
        if (this_tipo == -1){
            //Error:
            System.out.println("Error de tipos:"+this.toString(ti)+" / "+this.toString(td));
            System.exit(0);
        }
        
        n.setTipo(this_tipo);
        return this_tipo;
    }
    
    public int visit(ModNodo n){
        int ti = n.getPrimerHijo().accept(this);
        int td = n.getUltimoHijo().accept(this);
        int this_tipo = this.tipos.verificaMod(ti, td);
        
        if (this_tipo == -1){
            //Error:
            System.out.println("Error de tipos:"+this.toString(ti)+" % "+this.toString(td));
            System.exit(0);
        }
        
        n.setTipo(this_tipo);
        return this_tipo;
    }
    
    public int visit(PowNodo n){
        int ti = n.getPrimerHijo().accept(this);
        int td = n.getUltimoHijo().accept(this);
        int this_tipo = this.tipos.verificaPow(ti, td);
        
        if (this_tipo == -1){
            //Error:
            System.out.println("Error de tipos:"+this.toString(ti)+" ** "+this.toString(td));
            System.exit(0);
        }
        
        n.setTipo(this_tipo);
        return this_tipo;
    }
    
    public int visit(AndNodo n){
        int ti = n.getPrimerHijo().accept(this);
        int td = n.getUltimoHijo().accept(this);
        int this_tipo = this.tipos.verificaLogOp(ti, td);
        
        if (this_tipo == -1){
            //Error:
            System.out.println("Error de tipos:"+this.toString(ti)+" and "+this.toString(td));
            System.exit(0);
        }
        
        n.setTipo(this_tipo);
        return this_tipo;
    }
    
    public int visit(DiffNodo n){
        int ti = n.getPrimerHijo().accept(this);
        int td = n.getUltimoHijo().accept(this);
        int this_tipo = this.tipos.verificaCond(ti, td);
        
        if (this_tipo == -1){
            //Error:
            System.out.println("Error de tipos:"+this.toString(ti)+" != "+this.toString(td));
            System.exit(0);
        }
        
        n.setTipo(this_tipo);
        return this_tipo;
    }
    
    public int visit(EqNodo n){
        int ti = n.getPrimerHijo().accept(this);
        int td = n.getUltimoHijo().accept(this);
        int this_tipo = this.tipos.verificaCond(ti, td);
        
        if (this_tipo == -1){
            //Error:
            System.out.println("Error de tipos:"+this.toString(ti)+" == "+this.toString(td));
            System.exit(0);
        }
        
        n.setTipo(this_tipo);
        return this_tipo;
    }
    
    public int visit(GrNodo n){
        int ti = n.getPrimerHijo().accept(this);
        int td = n.getUltimoHijo().accept(this);
        int this_tipo = this.tipos.verificaCond(ti, td);
        
        if (this_tipo == -1){
            //Error:
            System.out.println("Error de tipos:"+this.toString(ti)+" > "+this.toString(td));
            System.exit(0);
        }
        
        
        n.setTipo(this_tipo);
        return this_tipo;
    }
    
    public int visit(GrqNodo n){
        int ti = n.getPrimerHijo().accept(this);
        int td = n.getUltimoHijo().accept(this);
        int this_tipo = this.tipos.verificaCond(ti, td);
        
        if (this_tipo == -1){
            //Error:
            System.out.println("Error de tipos:"+this.toString(ti)+" >= "+this.toString(td));
            System.exit(0);
        }
        
        n.setTipo(this_tipo);
        return this_tipo;
    }
    
    public int visit(LeqNodo n){
        int ti = n.getPrimerHijo().accept(this);
        int td = n.getUltimoHijo().accept(this);
        int this_tipo = this.tipos.verificaCond(ti, td);
        
        if (this_tipo == -1){
            //Error:
            System.out.println("Error de tipos:"+this.toString(ti)+" <= "+this.toString(td));
            System.exit(0);
        }
        
        n.setTipo(this_tipo);
        return this_tipo;
    }
    
    public int visit(LessNodo n){
        int ti = n.getPrimerHijo().accept(this);
        int td = n.getUltimoHijo().accept(this);
        int this_tipo = this.tipos.verificaCond(ti, td);
        
        if (this_tipo == -1){
            //Error:
            System.out.println("Error de tipos:"+this.toString(ti)+" < "+this.toString(td));
            System.exit(0);
        }
        
        n.setTipo(this_tipo);
        return this_tipo;
    }
    
    public int visit(NotNodo n){
        n.getPrimerHijo().accept(this);
        n.setTipo(0);
        return 0; //siempre boolean
    }
    
    public int visit(OrNodo n){
        int ti = n.getPrimerHijo().accept(this);
        int td = n.getUltimoHijo().accept(this);
        int this_tipo = this.tipos.verificaLogOp(ti, td);
        
        if (this_tipo == -1){
            //Error:
            System.out.println("Error de tipos:"+this.toString(ti)+" or "+this.toString(td));
            System.exit(0);
        }
        
        n.setTipo(this_tipo);
        return this_tipo;
    }
    
    public int visit(PrintNodo n){
        n.getPrimerHijo().accept(this);
        return 0;
    }
    
    public int visit(IfNodo n){
        if(n.getSizeHijos()==3){
            LinkedList<Nodo> hijos = n.getAll();
            hijos.get(0).accept(this);
            hijos.get(1).accept(this);
            hijos.get(2).accept(this);
        }else{
            LinkedList<Nodo> hijos = n.getAll();
            hijos.get(0).accept(this);
            hijos.get(1).accept(this);
        }
        return 0;
    }
    
    public int visit(WhileNodo n){
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        return 0;
    }
    
    public int visit(IdentifierHoja n){
        //ver si esta en la tabla
        if ( this.tabla.lookup(n.getNombre()) == null ){ //se uso un identificador no asignado anteriormente
            System.out.println("Error:"+n.getNombre()+" variable no encontrada.");
            System.exit(0);
        }
        
        n.setTipo(this.tabla.lookup(n.getNombre()));
        return this.tabla.lookup(n.getNombre());
    }
    
    public int visit(IntHoja n){
        n.setTipo(1);
        return 1;
    }
    
    public int visit(BoolHoja n){
        n.setTipo(0);
        return 0;
    }
    
    public int visit(RealHoja n){
        n.setTipo(2);
        return 2;
    }
    
    public int visit(StringHoja n){
        n.setTipo(3);
        return 3;
    }
    
    public int visit(Compuesto n){
        for (Iterator i = n.getHijos().iterator(); i.hasNext(); ) {
            Nodo hijo = (Nodo) i.next();
            hijo.accept(this);
        }
        return 0;
    }
    public int visit(Hoja n){return 0;}
    public int visit(Nodo n){return 0;}
    public int visit(NodoBinario n){return 0;}
    public int visit(NodoStmts n){return 0;}
}

Hermanos Mala Muerte:

El proyecto escribe el codio en el archivo "out.txt"

Para el punto extra creamos el binario "genera". 
Para funcionar necesita estar en el mismo directorio que la ruta planeada de "out.txt" en el proyecto.
Este ejecutable ejecuta el proyecto, lo que hace que se genere "out.txt", si este ejecutable esta en el mismo
directorio que la ruta de "out.txt", definida en la variable output de la clase Compilador.java (por defecto es el directorio Proyecto_5),
entonces se procede a generar el archivo .asm en el mismo directorio que el archivo en codigo fuente.

Para ejecutar:
	1.- Dar a "genera" permiso para ser ejecutado.
	2.- ejecutar como sudo ./genera /ruta/a/archivo.p

Se creara un archivo en /ruta/a/archivo.asm , que se puede ejecutar con spim y es semanticamente equivalente a archivo.p

Este ejecutable fue probado en Ubuntu 16.04

package testmaven;
import java.io.*;

public class Test {

    public static void main (String[] args){
        try{
            PrintWriter writer;
            AnalizadorLexico al;
            String result;
            
            writer = new PrintWriter("out/fizzbuzz.plx");
            al = new AnalizadorLexico("src/main/resources/fizzbuzz.p");
            result = al.analiza();
            writer.println(result);
            writer.close();

            writer = new PrintWriter("out/fz_error_cadena.plx");
            al = new AnalizadorLexico("src/main/resources/fz_error_cadena.p");
            result = al.analiza();
            writer.println(result);
            writer.close();
            
            writer = new PrintWriter("out/fz_error_indentacion.plx");
            al = new AnalizadorLexico("src/main/resources/fz_error_indentacion.p");
            result = al.analiza();
            writer.println(result);
            writer.close();
            
            writer = new PrintWriter("out/fz_error_lexema.plx");
            al = new AnalizadorLexico("src/main/resources/fz_error_lexema.p");
            result = al.analiza();
            writer.println(result);
            writer.close();
            
            
        }catch(FileNotFoundException ex) {
            System.out.println(ex.getMessage() + " No se encontró el archivo;");
        }
    }
}

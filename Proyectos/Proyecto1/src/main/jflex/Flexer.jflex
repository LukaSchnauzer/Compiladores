
  
package testmaven;
import java.io.*;
import java.util.*;

%%

%class Flexer 
%public
%standalone
%unicode
%line
%type String

%{ 
String tokens = "";
int inden = 0;

Stack pila = new Stack();

/*Funcion que se encarga de la indentacion en el archivo*/
public boolean checa_inden(int inden){
    
    //Esto deberia de hacerse solo una vez en toda la ejecucion
    if(pila.empty())
        pila.push(0);

    int tope = (Integer)(pila.peek());
    
    if (tope == inden){
        /*NADA*/
        return true;
    }else if (tope < inden){
        tokens+="INDENTA("+inden+")";
        pila.push(inden); //Se creo un nuevo bloque de indentacion
        return true;
    }else{
        while(tope > inden){
            int a = (Integer)pila.pop();
            tokens+="DEINDENTA("+(Integer)(pila.peek())+")";
            tope = (Integer)(pila.peek());
            if (tope == 0){
                tokens += "\nError de Indentacion. Linea "+(yyline+1);
                return false;
            }
            if(tope == inden){
                return true;
            }
        }
        tokens += "\nError de Indentacion. Linea "+(yyline+1);
        return false;
    }
}

%}

%eofval{
    /*Vaciar la pila de indentacion*/
    tokens+="\n"; //Claridad
    while(!pila.empty())
        tokens += "DEINDENTA("+(Integer)pila.pop()+")";
    
    return tokens;
%eofval}

RESERVADA = "and"|"or"|"not"|"for"|"while"|"if"|"else"|"elif"|"print"
OPERADOR = "+"|"-"|"*"|"**"|"/"|"//"|"%"|"<"|">"|"<="|">="|"="|"!"|"=="
BOOLEANO = "True"|"False"
PUNTO   = \.
ENTERO  = [1-9][0-9]* | 0+
REAL = {PUNTO}[0-9]+|{ENTERO}{PUNTO}|{ENTERO}{PUNTO}0*{ENTERO}
IDENTIFICADOR = ([a-zA-Z]|"_")([a-zA-Z]|[0-9]|"_")*
CADENA = "\""[^\"\\\n]*"\""
CADENA_MAL = "\""[^\"\\\n]*\n
SEPARADOR = ":"

COMENTARIO = "#".*\n

%state INDENTACION

%%
<INDENTACION>{
" "     {inden+=1;}
\t      {inden+=4;}
.       {yypushback(1);
         if(!checa_inden(inden))
            return tokens;
         yybegin(YYINITIAL);
         inden=0;}
}

<YYINITIAL>{
\n                  {tokens+="SALTO\n";
                     yybegin(INDENTACION);}
{ENTERO}            {tokens+="ENTERO("+yytext() + ")";}
{RESERVADA}         {tokens+="RESERVADA("+yytext() + ")";}
{BOOLEANO}          {tokens+="BOOLEANO("+yytext() + ")";}
{CADENA_MAL}        {tokens+="Cadena Mal Formadas("+yytext() +"). Linea "+(yyline+1);
                     return tokens;}
{CADENA}            {tokens+="CADENA("+yytext() + ")";}
{IDENTIFICADOR}     {tokens+="IDENTIFICADOR("+yytext() + ")";}
{REAL}              {tokens+="REAL("+yytext() + ")";}
{OPERADOR}          {tokens+="OPERADOR("+yytext() + ")";}
{SEPARADOR}         {tokens+="SEPARADOR("+yytext() + ")";}
{COMENTARIO}        {/*IGNORA y*/ tokens+="SALTO\n";
                     yybegin(INDENTACION);}
" "                 {/*IGNORA*/}
\t                  {/*IGNORA*/}
.                   {tokens+="Lexema No Soportado("+yytext() + "). Linea "+(yyline+1);
                     return tokens;}
}
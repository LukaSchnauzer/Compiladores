//### This file created by BYACC 1.8(/Java extension  1.15)
//### Java capabilities added 7 Jan 97, Bob Jamison
//### Updated : 27 Nov 97  -- Bob Jamison, Joe Nieten
//###           01 Jan 98  -- Bob Jamison -- fixed generic semantic constructor
//###           01 Jun 99  -- Bob Jamison -- added Runnable support
//###           06 Aug 00  -- Bob Jamison -- made state variables class-global
//###           03 Jan 01  -- Bob Jamison -- improved flags, tracing
//###           16 May 01  -- Bob Jamison -- added custom stack sizing
//###           04 Mar 02  -- Yuval Oren  -- improved java performance, added options
//###           14 Mar 02  -- Tomas Hurka -- -d support, static initializer workaround
//### Please send bug reports to tom@hukatronic.cz
//### static char yysccsid[] = "@(#)yaccpar	1.8 (Berkeley) 01/20/90";



package asintactico;



//#line 2 "../../../../src/main/byaccj/parser.y"
  import java.lang.Math;
  import java.io.*;
//#line 20 "Parser.java"




public class Parser
{

boolean yydebug;        //do I want debug output?
int yynerrs;            //number of errors so far
int yyerrflag;          //was there an error?
int yychar;             //the current working character

//########## MESSAGES ##########
//###############################################################
// method: debug
//###############################################################
void debug(String msg)
{
  if (yydebug)
    System.out.println(msg);
}

//########## STATE STACK ##########
final static int YYSTACKSIZE = 500;  //maximum stack size
int statestk[] = new int[YYSTACKSIZE]; //state stack
int stateptr;
int stateptrmax;                     //highest index of stackptr
int statemax;                        //state when highest index reached
//###############################################################
// methods: state stack push,pop,drop,peek
//###############################################################
final void state_push(int state)
{
  try {
		stateptr++;
		statestk[stateptr]=state;
	 }
	 catch (ArrayIndexOutOfBoundsException e) {
     int oldsize = statestk.length;
     int newsize = oldsize * 2;
     int[] newstack = new int[newsize];
     System.arraycopy(statestk,0,newstack,0,oldsize);
     statestk = newstack;
     statestk[stateptr]=state;
  }
}
final int state_pop()
{
  return statestk[stateptr--];
}
final void state_drop(int cnt)
{
  stateptr -= cnt; 
}
final int state_peek(int relative)
{
  return statestk[stateptr-relative];
}
//###############################################################
// method: init_stacks : allocate and prepare stacks
//###############################################################
final boolean init_stacks()
{
  stateptr = -1;
  val_init();
  return true;
}
//###############################################################
// method: dump_stacks : show n levels of the stacks
//###############################################################
void dump_stacks(int count)
{
int i;
  System.out.println("=index==state====value=     s:"+stateptr+"  v:"+valptr);
  for (i=0;i<count;i++)
    System.out.println(" "+i+"    "+statestk[i]+"      "+valstk[i]);
  System.out.println("======================");
}


//########## SEMANTIC VALUES ##########
//public class ParserVal is defined in ParserVal.java


String   yytext;//user variable to return contextual strings
ParserVal yyval; //used to return semantic vals from action routines
ParserVal yylval;//the 'lval' (result) I got from yylex()
ParserVal valstk[];
int valptr;
//###############################################################
// methods: value stack push,pop,drop,peek.
//###############################################################
void val_init()
{
  valstk=new ParserVal[YYSTACKSIZE];
  yyval=new ParserVal();
  yylval=new ParserVal();
  valptr=-1;
}
void val_push(ParserVal val)
{
  if (valptr>=YYSTACKSIZE)
    return;
  valstk[++valptr]=val;
}
ParserVal val_pop()
{
  if (valptr<0)
    return new ParserVal();
  return valstk[valptr--];
}
void val_drop(int cnt)
{
int ptr;
  ptr=valptr-cnt;
  if (ptr<0)
    return;
  valptr = ptr;
}
ParserVal val_peek(int relative)
{
int ptr;
  ptr=valptr-relative;
  if (ptr<0)
    return new ParserVal();
  return valstk[ptr];
}
final ParserVal dup_yyval(ParserVal val)
{
  ParserVal dup = new ParserVal();
  dup.ival = val.ival;
  dup.dval = val.dval;
  dup.sval = val.sval;
  dup.obj = val.obj;
  return dup;
}
//#### end semantic value section ####
public final static short SALTO=257;
public final static short MAS=258;
public final static short MENOS=259;
public final static short POR=260;
public final static short POTENCIA=261;
public final static short DIV=262;
public final static short DEINDENTA=263;
public final static short INDENTA=264;
public final static short OTRO=265;
public final static short PRINT=266;
public final static short DIVENTERA=267;
public final static short MODULO=268;
public final static short EQ=269;
public final static short MASIGUAL=270;
public final static short MENOSIGUAL=271;
public final static short DISTINTO=272;
public final static short IGUAL=273;
public final static short MENORIGUAL=274;
public final static short MAYORIGUAL=275;
public final static short MENOR=276;
public final static short MAYOR=277;
public final static short PUNTOYCOMA=278;
public final static short DOSPUNTOS=279;
public final static short ELIF=280;
public final static short ELSE=281;
public final static short IF=282;
public final static short WHILE=283;
public final static short FOR=284;
public final static short NOT=285;
public final static short OR=286;
public final static short AND=287;
public final static short ENTERO=288;
public final static short REAL=289;
public final static short IDENTIFICADOR=290;
public final static short CADENA=291;
public final static short BOOLEANO=292;
public final static short YYERRCODE=256;
final static short yylhs[] = {                           -1,
    0,    0,    0,    0,    0,    2,    2,    1,    1,    3,
    5,    5,    6,    6,    7,    4,    4,    9,    9,    9,
   12,   12,   10,   11,   11,    8,   13,   13,   15,   15,
   14,   14,   17,   17,   16,   16,   18,   18,   20,   20,
   21,   21,   21,   21,   21,   21,   19,   19,   23,   23,
   23,   23,   22,   22,   25,   25,   25,   25,   25,   25,
   25,   25,   24,   24,   24,   26,   26,   27,   27,   27,
   27,   27,
};
final static short yylen[] = {                            2,
    0,    1,    2,    2,    1,    1,    2,    1,    1,    2,
    1,    1,    1,    3,    2,    1,    1,    4,    7,    8,
    4,    5,    4,    1,    4,    1,    1,    2,    2,    3,
    1,    2,    2,    3,    2,    1,    1,    2,    2,    3,
    1,    1,    1,    1,    1,    1,    1,    2,    2,    2,
    3,    3,    1,    2,    2,    2,    2,    2,    3,    3,
    3,    3,    2,    2,    1,    1,    3,    1,    1,    1,
    1,    1,
};
final static short yydefred[] = {                         0,
    0,    0,    0,    0,    0,    0,    0,   69,   71,   68,
   70,   72,    0,    0,    8,    9,    0,   11,   12,    0,
   16,   17,   26,    0,    0,   36,    0,    0,    0,   65,
    0,    3,   63,   64,   15,    0,    0,   35,    4,   10,
    0,    0,    0,    0,    0,   43,   46,   45,   44,   41,
   42,   38,    0,    0,    0,   48,    0,    0,    0,    0,
    0,    0,    0,    0,   14,   29,    0,   33,    0,    0,
    0,    0,   55,   58,   56,   57,    0,    0,    0,    0,
   67,    0,   24,    0,   23,   30,   34,   40,   51,   52,
   59,   62,   60,   61,    0,    0,    0,    0,    6,    0,
    0,    0,    0,   25,    7,    0,   19,    0,    0,   20,
   22,
};
final static short yydgoto[] = {                         13,
   14,  100,   15,   16,   17,   18,   19,   20,   21,   22,
   84,   98,   23,   24,   43,   25,   45,   26,   27,   52,
   53,   28,   56,   29,   61,   30,   31,
};
final static short yysindex[] = {                      -190,
 -190, -162, -162, -201, -201, -201, -201,    0,    0,    0,
    0,    0,    0, -190,    0,    0, -241,    0,    0, -238,
    0,    0,    0, -254, -246,    0,   41, -245, -126,    0,
 -218,    0,    0,    0,    0, -234, -225,    0,    0,    0,
 -201, -201, -230, -201, -228,    0,    0,    0,    0,    0,
    0,    0, -162, -162, -162,    0, -162, -162, -162, -162,
  -95, -162, -180, -180,    0,    0, -201,    0, -201,   41,
 -245, -245,    0,    0,    0,    0, -162, -162, -162, -162,
    0, -200,    0, -260,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0, -134, -201, -209, -208,    0, -145,
 -205, -180, -204,    0,    0, -180,    0, -180, -195,    0,
    0,
};
final static short yyrindex[] = {                       103,
  103,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,  103,    0,    0,    0,    0,    0, -153,
    0,    0,    0, -207,  -70,    0, -239, -250,  -75,    0,
  -98,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0, -140,    0,   -8,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
  -52,    0,    0,    0,    0,    0,    0,    0,    0,  -10,
  -31,   25,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    1,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0, -175,    0,
    0,
};
final static short yygindex[] = {                        14,
  -83,    0,  -53,    0,    0,    0,    0,   -2,    0,    0,
  -64,    6,    0,  -34,    0,    2,    0,    0,   54,   46,
    0,  -24,  -20,    3,    0,    0,    0,
};
final static int YYTABLESIZE=318;
static short yytable[];
static { yytable();}
static void yytable(){
yytable = new short[]{                         85,
   18,   35,   36,   37,   33,   34,   47,   66,   38,   83,
   83,   99,   54,   55,   32,   40,  105,   37,   47,   96,
   97,   47,   47,   47,   47,   47,   47,   39,   47,   71,
   72,   42,   86,   37,   41,   47,   47,  107,   65,   37,
   44,  109,   62,  110,   63,   68,   37,   37,   83,   27,
   89,   90,   83,   64,   83,   67,    2,    3,   69,   73,
   74,   75,   76,   95,   81,   27,    1,    2,    3,  102,
   87,   27,  103,  106,  108,    4,   82,    2,    3,   91,
   92,   93,   94,    7,   96,    4,    8,    9,   10,   11,
   12,    5,    6,  101,    7,    2,    3,    8,    9,   10,
   11,   12,    1,   13,    7,   21,   70,    8,    9,   10,
   11,   12,    2,    3,  111,   88,   28,  104,    0,    0,
    4,    0,    0,    2,    3,    8,    9,   10,   11,   12,
    0,    4,   28,   57,    0,   58,    5,    6,   28,    7,
   59,   60,    8,    9,   10,   11,   12,    5,    6,    0,
    7,    0,    0,    8,    9,   10,   11,   12,   66,   66,
   66,   66,    0,   66,   77,    0,   78,    0,   66,   66,
   66,   79,   80,   66,   66,   66,   66,   66,   66,    0,
   66,   53,   53,   53,    0,    0,   31,   66,   66,    0,
    0,    0,    0,   53,    0,    0,   53,   53,   53,   53,
   53,   53,   31,   53,   54,   54,   54,    0,   31,    0,
   53,   53,    0,    0,    0,   31,   54,    0,    0,   54,
   54,   54,   54,   54,   54,   49,   54,    0,    0,    0,
    0,    0,    0,   54,   54,    0,    0,   49,    0,    0,
   49,   49,   49,   49,   49,   49,   39,   49,   32,    0,
    0,    0,    0,    0,   49,   49,    0,   18,   18,   18,
    0,    0,   39,   18,   32,    0,   18,    0,   39,    0,
   32,    0,    0,    0,    0,   39,   39,   32,    0,    0,
    0,   50,   18,   18,    0,   18,    0,    0,   18,   18,
   18,   18,   18,   50,    0,    0,   50,   50,   50,   50,
   50,   50,    0,   50,    0,    0,    0,    0,    0,   46,
   50,   50,   47,    0,   48,   49,   50,   51,
};
}
static short yycheck[];
static { yycheck(); }
static void yycheck() {
yycheck = new short[] {                         64,
    0,    4,    5,    6,    2,    3,  257,   42,    7,   63,
   64,   95,  258,  259,    1,  257,  100,  257,  269,  280,
  281,  272,  273,  274,  275,  276,  277,   14,  279,   54,
   55,  286,   67,  273,  273,  286,  287,  102,   41,  279,
  287,  106,  261,  108,  279,   44,  286,  287,  102,  257,
   71,   72,  106,  279,  108,  286,  258,  259,  287,   57,
   58,   59,   60,  264,   62,  273,  257,  258,  259,  279,
   69,  279,  281,  279,  279,  266,  257,  258,  259,   77,
   78,   79,   80,  285,  280,  266,  288,  289,  290,  291,
  292,  282,  283,   96,  285,  258,  259,  288,  289,  290,
  291,  292,    0,  257,  285,  281,   53,  288,  289,  290,
  291,  292,  258,  259,  109,   70,  257,  263,   -1,   -1,
  266,   -1,   -1,  258,  259,  288,  289,  290,  291,  292,
   -1,  266,  273,  260,   -1,  262,  282,  283,  279,  285,
  267,  268,  288,  289,  290,  291,  292,  282,  283,   -1,
  285,   -1,   -1,  288,  289,  290,  291,  292,  257,  258,
  259,  260,   -1,  262,  260,   -1,  262,   -1,  267,  268,
  269,  267,  268,  272,  273,  274,  275,  276,  277,   -1,
  279,  257,  258,  259,   -1,   -1,  257,  286,  287,   -1,
   -1,   -1,   -1,  269,   -1,   -1,  272,  273,  274,  275,
  276,  277,  273,  279,  257,  258,  259,   -1,  279,   -1,
  286,  287,   -1,   -1,   -1,  286,  269,   -1,   -1,  272,
  273,  274,  275,  276,  277,  257,  279,   -1,   -1,   -1,
   -1,   -1,   -1,  286,  287,   -1,   -1,  269,   -1,   -1,
  272,  273,  274,  275,  276,  277,  257,  279,  257,   -1,
   -1,   -1,   -1,   -1,  286,  287,   -1,  257,  258,  259,
   -1,   -1,  273,  263,  273,   -1,  266,   -1,  279,   -1,
  279,   -1,   -1,   -1,   -1,  286,  287,  286,   -1,   -1,
   -1,  257,  282,  283,   -1,  285,   -1,   -1,  288,  289,
  290,  291,  292,  269,   -1,   -1,  272,  273,  274,  275,
  276,  277,   -1,  279,   -1,   -1,   -1,   -1,   -1,  269,
  286,  287,  272,   -1,  274,  275,  276,  277,
};
}
final static short YYFINAL=13;
final static short YYMAXTOKEN=292;
final static String yyname[] = {
"end-of-file",null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,"SALTO","MAS","MENOS","POR","POTENCIA","DIV","DEINDENTA",
"INDENTA","OTRO","PRINT","DIVENTERA","MODULO","EQ","MASIGUAL","MENOSIGUAL",
"DISTINTO","IGUAL","MENORIGUAL","MAYORIGUAL","MENOR","MAYOR","PUNTOYCOMA",
"DOSPUNTOS","ELIF","ELSE","IF","WHILE","FOR","NOT","OR","AND","ENTERO","REAL",
"IDENTIFICADOR","CADENA","BOOLEANO",
};
final static String yyrule[] = {
"$accept : input",
"input :",
"input : SALTO",
"input : SALTO input",
"input : stmt input",
"input : stmt",
"aux00 : stmt",
"aux00 : aux00 stmt",
"stmt : simple_stmt",
"stmt : compound_stmt",
"simple_stmt : small_stmt SALTO",
"small_stmt : expr_stmt",
"small_stmt : print_stmt",
"expr_stmt : test",
"expr_stmt : test IGUAL test",
"print_stmt : PRINT test",
"compound_stmt : if_stmt",
"compound_stmt : while_stmt",
"if_stmt : IF test DOSPUNTOS suite",
"if_stmt : IF test DOSPUNTOS suite ELSE DOSPUNTOS suite",
"if_stmt : IF test DOSPUNTOS suite aux3 ELSE DOSPUNTOS suite",
"aux3 : ELIF test DOSPUNTOS suite",
"aux3 : ELIF test DOSPUNTOS suite aux3",
"while_stmt : WHILE test DOSPUNTOS suite",
"suite : simple_stmt",
"suite : SALTO INDENTA aux00 DEINDENTA",
"test : or_test",
"or_test : and_test",
"or_test : and_test aux4",
"aux4 : OR and_test",
"aux4 : aux4 OR and_test",
"and_test : not_test",
"and_test : not_test aux5",
"aux5 : AND not_test",
"aux5 : aux5 AND not_test",
"not_test : NOT not_test",
"not_test : comparison",
"comparison : expr",
"comparison : expr aux6",
"aux6 : comp_op expr",
"aux6 : comp_op expr aux6",
"comp_op : MENOR",
"comp_op : MAYOR",
"comp_op : EQ",
"comp_op : MAYORIGUAL",
"comp_op : MENORIGUAL",
"comp_op : DISTINTO",
"expr : term",
"expr : term aux7",
"aux7 : MAS term",
"aux7 : MENOS term",
"aux7 : MAS term aux7",
"aux7 : MENOS term aux7",
"term : factor",
"term : factor aux8",
"aux8 : POR factor",
"aux8 : DIVENTERA factor",
"aux8 : MODULO factor",
"aux8 : DIV factor",
"aux8 : aux8 POR factor",
"aux8 : aux8 DIVENTERA factor",
"aux8 : aux8 MODULO factor",
"aux8 : aux8 DIV factor",
"factor : MAS factor",
"factor : MENOS factor",
"factor : power",
"power : atom",
"power : atom POTENCIA factor",
"atom : IDENTIFICADOR",
"atom : ENTERO",
"atom : CADENA",
"atom : REAL",
"atom : BOOLEANO",
};

//#line 155 "../../../../src/main/byaccj/parser.y"
/* referencia al generadores de analizadores léxicos */
private Flexer lexer;


private int yylex () {
    int yyl_return = -1;
    try {
      yyl_return = lexer.yylex();
    }
    catch (IOException e) {
      System.err.println("IO error :"+e);
    }
    return yyl_return;
}


public void yyerror (String error) {
   System.err.println ("[ERROR]  " + error);
   System.exit(1);
}

/* lexer es creado en el constructor */
public Parser(Reader r) {
    lexer = new Flexer(r, this);
    //yydebug = true;
}

/* Uso del parser */
/* Creación del parser e inicialización del reconocimiento */
public static void main(String args[]) throws IOException {
    Parser parser = new Parser(new FileReader("src/main/resources/test.txt"));
    parser.yydebug = true;
    parser.yyparse();
}
//#line 411 "Parser.java"
//###############################################################
// method: yylexdebug : check lexer state
//###############################################################
void yylexdebug(int state,int ch)
{
String s=null;
  if (ch < 0) ch=0;
  if (ch <= YYMAXTOKEN) //check index bounds
     s = yyname[ch];    //now get it
  if (s==null)
    s = "illegal-symbol";
  debug("state "+state+", reading "+ch+" ("+s+")");
}





//The following are now global, to aid in error reporting
int yyn;       //next next thing to do
int yym;       //
int yystate;   //current parsing state from state table
String yys;    //current token string


//###############################################################
// method: yyparse : parse input and execute indicated items
//###############################################################
int yyparse()
{
boolean doaction;
  init_stacks();
  yynerrs = 0;
  yyerrflag = 0;
  yychar = -1;          //impossible char forces a read
  yystate=0;            //initial state
  state_push(yystate);  //save it
  val_push(yylval);     //save empty value
  while (true) //until parsing is done, either correctly, or w/error
    {
    doaction=true;
    if (yydebug) debug("loop"); 
    //#### NEXT ACTION (from reduction table)
    for (yyn=yydefred[yystate];yyn==0;yyn=yydefred[yystate])
      {
      if (yydebug) debug("yyn:"+yyn+"  state:"+yystate+"  yychar:"+yychar);
      if (yychar < 0)      //we want a char?
        {
        yychar = yylex();  //get next token
        if (yydebug) debug(" next yychar:"+yychar);
        //#### ERROR CHECK ####
        if (yychar < 0)    //it it didn't work/error
          {
          yychar = 0;      //change it to default string (no -1!)
          if (yydebug)
            yylexdebug(yystate,yychar);
          }
        }//yychar<0
      yyn = yysindex[yystate];  //get amount to shift by (shift index)
      if ((yyn != 0) && (yyn += yychar) >= 0 &&
          yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
        {
        if (yydebug)
          debug("state "+yystate+", shifting to state "+yytable[yyn]);
        //#### NEXT STATE ####
        yystate = yytable[yyn];//we are in a new state
        state_push(yystate);   //save it
        val_push(yylval);      //push our lval as the input for next rule
        yychar = -1;           //since we have 'eaten' a token, say we need another
        if (yyerrflag > 0)     //have we recovered an error?
           --yyerrflag;        //give ourselves credit
        doaction=false;        //but don't process yet
        break;   //quit the yyn=0 loop
        }

    yyn = yyrindex[yystate];  //reduce
    if ((yyn !=0 ) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
      {   //we reduced!
      if (yydebug) debug("reduce");
      yyn = yytable[yyn];
      doaction=true; //get ready to execute
      break;         //drop down to actions
      }
    else //ERROR RECOVERY
      {
      if (yyerrflag==0)
        {
        yyerror("syntax error");
        yynerrs++;
        }
      if (yyerrflag < 3) //low error count?
        {
        yyerrflag = 3;
        while (true)   //do until break
          {
          if (stateptr<0)   //check for under & overflow here
            {
            yyerror("stack underflow. aborting...");  //note lower case 's'
            return 1;
            }
          yyn = yysindex[state_peek(0)];
          if ((yyn != 0) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
            if (yydebug)
              debug("state "+state_peek(0)+", error recovery shifting to state "+yytable[yyn]+" ");
            yystate = yytable[yyn];
            state_push(yystate);
            val_push(yylval);
            doaction=false;
            break;
            }
          else
            {
            if (yydebug)
              debug("error recovery discarding state "+state_peek(0)+" ");
            if (stateptr<0)   //check for under & overflow here
              {
              yyerror("Stack underflow. aborting...");  //capital 'S'
              return 1;
              }
            state_pop();
            val_pop();
            }
          }
        }
      else            //discard this token
        {
        if (yychar == 0)
          return 1; //yyabort
        if (yydebug)
          {
          yys = null;
          if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
          if (yys == null) yys = "illegal-symbol";
          debug("state "+yystate+", error recovery discards token "+yychar+" ("+yys+")");
          }
        yychar = -1;  //read another
        }
      }//end error recovery
    }//yyn=0 loop
    if (!doaction)   //any reason not to proceed?
      continue;      //skip action
    yym = yylen[yyn];          //get count of terminals on rhs
    if (yydebug)
      debug("state "+yystate+", reducing "+yym+" by rule "+yyn+" ("+yyrule[yyn]+")");
    if (yym>0)                 //if count of rhs not 'nil'
      yyval = val_peek(yym-1); //get current semantic value
    yyval = dup_yyval(yyval); //duplicate yyval if ParserVal is used as semantic value
    switch(yyn)
      {
//########## USER-SUPPLIED ACTIONS ##########
case 1:
//#line 14 "../../../../src/main/byaccj/parser.y"
{ System.out.println("Reconocimiento Exitoso");}
break;
case 4:
//#line 17 "../../../../src/main/byaccj/parser.y"
{ System.out.println("Reconocimiento Exitoso");}
break;
case 5:
//#line 18 "../../../../src/main/byaccj/parser.y"
{ System.out.println("Reconocimiento Exitoso");}
break;
//#line 572 "Parser.java"
//########## END OF USER-SUPPLIED ACTIONS ##########
    }//switch
    //#### Now let's reduce... ####
    if (yydebug) debug("reduce");
    state_drop(yym);             //we just reduced yylen states
    yystate = state_peek(0);     //get new state
    val_drop(yym);               //corresponding value drop
    yym = yylhs[yyn];            //select next TERMINAL(on lhs)
    if (yystate == 0 && yym == 0)//done? 'rest' state and at first TERMINAL
      {
      if (yydebug) debug("After reduction, shifting from state 0 to state "+YYFINAL+"");
      yystate = YYFINAL;         //explicitly say we're done
      state_push(YYFINAL);       //and save it
      val_push(yyval);           //also save the semantic value of parsing
      if (yychar < 0)            //we want another character?
        {
        yychar = yylex();        //get next character
        if (yychar<0) yychar=0;  //clean, if necessary
        if (yydebug)
          yylexdebug(yystate,yychar);
        }
      if (yychar == 0)          //Good exit (if lex returns 0 ;-)
         break;                 //quit the loop--all DONE
      }//if yystate
    else                        //else not done yet
      {                         //get next state and push, for next yydefred[]
      yyn = yygindex[yym];      //find out where to go
      if ((yyn != 0) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn]; //get new state
      else
        yystate = yydgoto[yym]; //else go to new defred
      if (yydebug) debug("after reduction, shifting from state "+state_peek(0)+" to state "+yystate+"");
      state_push(yystate);     //going again, so push state & val...
      val_push(yyval);         //for next action
      }
    }//main loop
  return 0;//yyaccept!!
}
//## end of method parse() ######################################



//## run() --- for Thread #######################################
/**
 * A default run method, used for operating this parser
 * object in the background.  It is intended for extending Thread
 * or implementing Runnable.  Turn off with -Jnorun .
 */
public void run()
{
  yyparse();
}
//## end of method run() ########################################



//## Constructors ###############################################
/**
 * Default constructor.  Turn off with -Jnoconstruct .

 */
public Parser()
{
  //nothing to do
}


/**
 * Create a parser, setting the debug to true or false.
 * @param debugMe true for debugging, false for no debug.
 */
public Parser(boolean debugMe)
{
  yydebug=debugMe;
}
//###############################################################



}
//################### END OF CLASS ##############################

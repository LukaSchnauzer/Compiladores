%{
  import java.lang.Math;
  import java.io.*;
%}
/* Átomos del lenguaje */
%token SALTO MAS MENOS POR POTENCIA DIV DEINDENTA INDENTA OTRO PRINT DIVENTERA MODULO EQ MASIGUAL MENOSIGUAL DISTINTO IGUAL MENORIGUAL MAYORIGUAL MENOR MAYOR EQ PUNTOYCOMA DOSPUNTOS ELIF ELSE IF WHILE FOR NOT OR AND 
%token<ival> ENTERO
%token<dval> REAL
%token<sval> IDENTIFICADOR CADENA 


/* Producciones */
%%
input: { System.out.println("Reconocimiento Exitoso");}
     | SALTO
     | SALTO input
     | stmt input { System.out.println("Reconocimiento Exitoso");}
     | stmt{ System.out.println("Reconocimiento Exitoso");}
     
;

aux00: stmt
      | aux00 stmt
;



stmt: simple_stmt
      | compound_stmt
;

simple_stmt: small_stmt SALTO
;

small_stmt: expr_stmt
            | print_stmt
;
expr_stmt: test
        | test IGUAL test
;




print_stmt: PRINT test
;

compound_stmt: if_stmt
                | while_stmt


if_stmt: IF test DOSPUNTOS suite
          | IF test DOSPUNTOS suite ELSE DOSPUNTOS suite
          | IF test DOSPUNTOS suite aux3 ELSE DOSPUNTOS suite
;
aux3: ELIF test DOSPUNTOS suite
      | ELIF test DOSPUNTOS suite aux3
;

while_stmt: WHILE test DOSPUNTOS suite
;

suite: simple_stmt 
      | SALTO INDENTA aux00 DEINDENTA
;


test: or_test
;

or_test: and_test
          | and_test aux4
;

aux4: OR and_test
      | aux4 OR and_test
;

and_test: not_test
          | not_test aux5
;
aux5: AND not_test
      | aux5 AND not_test
;
not_test: NOT not_test
          | comparison
;
comparison: expr
            | expr aux6
;

aux6: comp_op expr
      | comp_op expr aux6
;

comp_op: MENOR
        | MAYOR
        | EQ
        | MAYORIGUAL
        | MENORIGUAL
        | DISTINTO
        
;
expr: term
      | term aux7
;
aux7: MAS term
      | MENOS term
      | MAS term aux7
      | MENOS term aux7
;

term:  factor
      | factor aux8
;

aux8:  POR factor
     | DIVENTERA factor
     | MODULO factor
     | DIV factor
     | aux8 POR factor 
     | aux8 DIVENTERA factor
     | aux8 MODULO factor
     | aux8 DIV factor
;


factor: MAS factor
  | MENOS factor
  | power
;


power:  atom
      | atom POTENCIA factor
; 


atom:  IDENTIFICADOR
     | ENTERO
     | CADENA
     | REAL
     | BOOLEANO
;






%%
/* referencia al generadores de analizadores léxicos */
private Flexer lexer;


private int yylex () {
    int yyl_return = -1;
    try {
      yyl_return = lexer.yylex();
    }
    catch (IOException e) {
      System.err.println("IO error :"+e);
    }
    return yyl_return;
}


public void yyerror (String error) {
   System.err.println ("[ERROR]  " + error);
   System.exit(1);
}

/* lexer es creado en el constructor */
public Parser(Reader r) {
    lexer = new Flexer(r, this);
    //yydebug = true;
}

/* Uso del parser */
/* Creación del parser e inicialización del reconocimiento */
public static void main(String args[]) throws IOException {
    Parser parser = new Parser(new FileReader("src/main/resources/test.txt"));
    parser.yydebug = true;
    parser.yyparse();
}
